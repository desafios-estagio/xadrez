#include <iostream>

using namespace std;

int main()
{
    int peca[6];
    int tabulerio[8][8]={
        4,3,2,5,6,2,3,4,
        1,1,1,1,1,1,1,1,
        0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,
        1,1,1,1,1,1,1,1,
        4,3,2,5,6,2,3,4
        };
    for(int i=0;i<6;i++){
        peca[i]=0;
    }
    for(int i=0;i<8;i++){
        for(int j=0;j<8;j++){
                peca[0] += (tabulerio[i][j]&&(tabulerio[i][j] == 1));
                peca[1] += (tabulerio[i][j]&&(tabulerio[i][j] == 2));
                peca[2] += (tabulerio[i][j]&&(tabulerio[i][j] == 3));
                peca[3] += (tabulerio[i][j]&&(tabulerio[i][j] == 4));
                peca[4] += (tabulerio[i][j]&&(tabulerio[i][j] == 5));
                peca[5] += (tabulerio[i][j]&&(tabulerio[i][j] == 6));
        }
    }
    cout << "Peao:" << peca[0] << " pecas(s)"<< endl;
    cout << "Bispo:" << peca[1] << " pecas(s)"<< endl;
    cout << "Cavalo:" << peca[2] << " pecas(s)"<< endl;
    cout << "Torre:" << peca[3] << " pecas(s)"<< endl;
    cout << "Rainha:" << peca[4] << " pecas(s)"<< endl;
    cout << "Rei:" << peca[5] << " pecas(s)"<< endl;
}
